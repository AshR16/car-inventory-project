let problem4 = require("./problem4.js")

function problem5(inventory,year){

    let carData = problem4(inventory)
    let olderCar = []
    for(let index =0; index < carData.length ;index++){
        if(carData[index] < year){
            olderCar.push(carData[index])
        }
        
    }

return olderCar
    
}

module.exports = problem5
function problem3(inventory){
    
    return inventory.sort((a,b)=>{
        let firstName = a.car_model.toUpperCase()
        let secondName = b.car_model.toUpperCase()
        
        if(firstName > secondName){
            return 1
        }else if(firstName < secondName){
            return -1
        }else{
            return 0
        }
    })
    
}

module.exports = problem3
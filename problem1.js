function problem1(inventory, carId) {
    if(inventory == undefined || inventory.length == 0 || carId == undefined){
        return [];
    }
    for (let index=0; index < inventory.length; index++){
        if (inventory[index].id === carId){
            return `Car ${carId} is a ${inventory[index].car_year} ${inventory[index].car_make} ${inventory[index].car_model}`
        }
    }
}


module.exports = problem1